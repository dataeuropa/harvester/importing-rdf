package io.piveau.importing.rdf

import io.piveau.pipe.PipeContext
import io.piveau.rdf.*
import io.piveau.utils.JenaUtils
import io.piveau.utils.gunzip
import io.vertx.circuitbreaker.CircuitBreaker
import io.vertx.circuitbreaker.CircuitBreakerOptions
import io.vertx.core.Vertx
import io.vertx.core.file.OpenOptions
import io.vertx.core.http.HttpHeaders
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.codec.BodyCodec
import io.vertx.kotlin.coroutines.await
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.riot.Lang
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.RDF
import org.slf4j.LoggerFactory
import java.io.File
import java.util.concurrent.TimeUnit

data class Page(val page: Model, val total: Int)
data class Dataset(val dataset: Model, val dataInfo: JsonObject)

class DownloadSource(private val vertx: Vertx, private val client: WebClient, config: JsonObject) {

    private val log = LoggerFactory.getLogger(javaClass)

    private val preProcessing = config.getBoolean("PIVEAU_IMPORTING_PREPROCESSING", false)

    private val circuitBreaker = CircuitBreaker
        .create("importing", vertx, CircuitBreakerOptions().setMaxRetries(2).setTimeout(180000))
        .retryPolicy { _, c -> c * 2000L }

    private val executor = vertx.createSharedWorkerExecutor("modelReader", 20, 10, TimeUnit.MINUTES)

    fun pagesFlow(address: String, pipeContext: PipeContext): Flow<Page> = flow {
        var nextLink: String? = address
        val accept = pipeContext.config.getString("accept")
        val inputFormat = pipeContext.config.getString("inputFormat")
        val applyPreProcessing = pipeContext.config.getBoolean("preProcessing", preProcessing)
        val brokenHydra = pipeContext.config.getBoolean("brokenHydra", false)

        do {
            val tmpFileName: String = vertx.fileSystem().createTempFile("tmp", "piveau", ".tmp", null as String?).await()
            log.trace("Temp file name: {}", tmpFileName)
            try {
                val stream = vertx.fileSystem().open(tmpFileName, OpenOptions().setWrite(true)).await()
                log.trace("Temp file opened")

                log.debug("Next address: {}", nextLink)
                val request = client.getAbs(nextLink as String).`as`(BodyCodec.pipe(stream, true))
                if (accept != null) {
                    request.putHeader("Accept", accept)
                }

                val response = request.timeout(120000).send().await()

                val finalFileName = if (address.endsWith(".gz") || response.headers()["Content-Type"].contains("application/gzip")) {
                    val targetFileName: String = vertx.fileSystem().createTempFile("tmp", "piveau", ".tmp", null as String?).await()
                    gunzip(tmpFileName, targetFileName)
                    targetFileName
                } else tmpFileName

                nextLink = when (response.statusCode()) {
                    in 200..299 -> {
                        val contentType = inputFormat ?: response.getHeader(HttpHeaders.CONTENT_TYPE.toString()) ?: "application/rdf+xml"
                        if (contentType.isRDF) {

                            val (fileName, content, finalContentType) = if (applyPreProcessing) {
                                val output = vertx.fileSystem().createTempFile("tmp", "piveau", ".tmp", null as String?).await()
                                val (_, finalContentType) = preProcess(
                                    File(finalFileName).inputStream(),
                                    File(output).outputStream(),
                                    contentType,
                                    address
                                )
                                Triple(output, File(output).inputStream(), finalContentType)
                            } else {
                                Triple("", File(finalFileName).inputStream(), contentType)
                            }

                            val page = try {
                                executor.executeBlocking { promise ->
                                    try {
                                        val model = JenaUtils.read(content, finalContentType, address)
                                        promise.complete(model)
                                    } catch (e: Exception) {
                                        promise.fail(e)
                                    }
                                }.await()
                            } catch (e: Exception) {
                                throw Throwable("$nextLink: ${e.message}")
                            }

                            if (fileName.isNotBlank()) {
                                vertx.fileSystem().delete(fileName)
                            }

                            val hydraPaging = HydraPaging.findPaging(page, if (brokenHydra) address else null)

                            val next = hydraPaging.next

                            emit(Page(page, hydraPaging.total))

                            next

                        } else {
                            throw Throwable("$nextLink: Content-Type $contentType is not an RDF content type. Content:\n${response.bodyAsString()}")
                        }
                    }
                    else -> {
                        throw Throwable("$nextLink: ${response.statusCode()} - ${response.statusMessage()}\n${response.bodyAsString()}")
                    }
                }
                if (finalFileName != tmpFileName) {
                    vertx.fileSystem().delete(finalFileName)
                }
            } finally {
                log.debug("Deleting temp file {}", tmpFileName)
                vertx.fileSystem().delete(tmpFileName)
            }

        } while (nextLink != null)
    }

    fun datasetsFlow(page: Page, pipeContext: PipeContext): Flow<Dataset> = flow {
        val removePrefix = pipeContext.config.getBoolean("removePrefix", false)
        val precedenceUriRef = pipeContext.config.getBoolean("precedenceUriRef", false)

        if (pipeContext.config.getBoolean("useTempFile", false)) {
            throw NotImplementedError("Using temp file is currently not supported")
        } else {
            // cleanup some idiosyncrasies
            page.page.removeAll(null, RDF.type, DCAT.dataset)

            val datasets = page.page.listResourcesWithProperty(RDF.type, DCAT.Dataset).toSet()
            val total = if (page.total > 0) page.total else datasets.size
            datasets.forEach { dataset ->
                dataset.identify(removePrefix, precedenceUriRef)?.let { id ->
                    if (id.isNotBlank()) {
                        val dataInfo = JsonObject()
                            .put("total", total)
                            .put("identifier", id)

                        val datasetModel = dataset.extractAsModel() ?: ModelFactory.createDefaultModel()

                        // More idiosyncrasies
                        datasetModel.listStatements(null, RDF.type, DCAT.Catalog).toList().forEach { stmt ->
                            stmt.subject.extractAsModel()?.let { model ->
                                datasetModel.remove(model)
                            }
                        }
                        if (!datasetModel.isEmpty) {
                            emit(Dataset(datasetModel, dataInfo))
                        }
                    } else {
                        pipeContext.log.warn("Could not extract an identifier from dataset")
                        if (pipeContext.log.isDebugEnabled) {
                            pipeContext.log.debug(
                                dataset.extractAsModel()?.presentAs(Lang.TURTLE) ?: "no model"
                            )
                        }
                    }
                } ?: pipeContext.log.warn("Could not extract an identifier from dataset")
            }
        }
    }

}
