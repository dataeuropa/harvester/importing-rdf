# Pipe Segment Configuration

## _mandatory_

* `address`

  Address of the source

* `catalogue`

  The id of the target catalogue

## _optional_

* `removePrefix`

  Try to remove prefix from uriRefs and take everything after last `/` as identifier. Default is `false`.

* `precedenceUriRef`

  Give uriRef precedence over `dct:identifier` as identifier. Default is `false`.

* `inputFormat`

  Mimetype to read from source. Takes precedence over header `Content-Type`

* `outputFormat`

  Mimetype to use for payload. Default is `application/n-triples`

  Possible output formats are:

    * `application/rdf+xml`
    * `application/n-triples`
    * `application/ld+json`
    * `application/trig`
    * `text/turtle`
    * `text/n3`

* `brokenHydra`

  Some sources use a wrong urls in hydra information for paging. If set to true the service will try to handle such broken hydra information. Default is `false`

* `sendListDelay`

  The delay in milliseconds before the list of identifiers is send. Take precedence over service configuration (see `PVEAU_IMPORTING_SEND_LIST_DELAY`)

* `preProcessing`

  Any pre-processing (e.g. URI encoding fixes) should take place. Overwrites importers configuration (see `PIVEAU_IMPORTING_PREPROCESSING`)

# Data Info Object

* `total`

  Total number of datasets

* `counter`

  The number of this dataset

* `identifier`

  The unique identifier in the source of this dataset

* `catalogue`

  The id of the target catalogue
